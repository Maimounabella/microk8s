<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function index(Request $request)
    {
        $message = "Bienvenue sur mon site web !";
        return view('message', ['message' => $message]);
    }
}