<?php
namespace App\Http\Controllers;

// routes/web.php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MessageController;

Route::get('/', [MessageController::class, 'index']);
// app/Http/Controllers/MessageController.php

use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function index(Request $request)
    {
        $message = "Bienvenue sur mon site web !";
        return view('message', ['message' => $message]);
    }
}