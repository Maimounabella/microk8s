## Présentation du projet

 Creation d'une application web simple auquel. J'ai utilisé le langage PHP et le framework Laravel pour créer une application fonctionnelle répondant à une requête HTTP de base. De plus, j'ai conteneurisé l'application à l'aide de Docker, déployé sur Kubernetes, configuré un Ingress et mis en place un pipeline d'intégration et de livraison continues avec GitLab CI.

## Construction de l'Application Web

J'ai construit l'application web en utilisant PHP et le framework Laravel. Les fichiers clés du projet sont les suivants :

1. `routes/web.php` : Ce fichier définit les routes de l'application, c'est-à-dire les URL permettant d'accéder aux différentes fonctionnalités. Par exemple, j'ai pu définir une route pour la page d'accueil de l'application.

2. `app/Http/Controllers/MessageController.php` : Ce fichier contient le contrôleur de l'application. J'ai écrit la logique pour traiter les requêtes et préparer les données nécessaires pour les vues.

3. `resources/views/message.blade.php` : Ce fichier est la vue de l'application, définissant la structure et le contenu de la page HTML renvoyée au client. J'ai utilisé le langage de modèle Blade fourni par Laravel pour inclure des variables et des structures de contrôle.

## Conteneurisation avec Docker

J'ai utilisé Docker pour conteneuriser l'application, garantissant ainsi sa portabilité et sa facilité de déploiement. Le fichier clé lié à Docker dans le projet est le suivant :

1. `Dockerfile` : J'ai créé ce fichier pour décrire les étapes nécessaires à la création de l'image Docker de l'application. J'ai spécifié l'image de base, copié les fichiers source de l'application, installé PHP et les dépendances nécessaires, et exécuté la commande `php", "artisan", "serve", "--host=0.0.0.0", "--port=80` pour démarrer le serveur web intégré de Laravel.

## Déploiement sur Kubernetes avec Ingress

J'ai déployé l'application sur un cluster Kubernetes et configuré un Ingress pour permettre l'accès à l'application depuis l'extérieur. Les fichiers liés au déploiement sur Kubernetes dans le projet sont les suivants :

1. `deployment.yaml` : J'ai créé ce fichier YAML pour définir un déploiement Kubernetes de l'application. J'ai spécifié le nombre de réplicas de l'application, les conteneurs à utiliser, les volumes nécessaires, etc.

2. `service.yaml` : J'ai créé ce fichier YAML pour définir un service Kubernetes pour l'application. Il spécifie comment l'application peut être accédée à l'intérieur du cluster.

3. `ingress.yaml` : J'ai créé ce fichier YAML pour définir une ressource Kubernetes appelée Ingress. Cela permet de configurer les règles de routage pour accéder à l'application depuis l'extérieur du cluster.

## Pipeline d'Intégration et de Livraison Continues avec GitLab CI

GitLab CI est utilisé pour mettre en place un pipeline d'intégration et de livraison continues pour le projet. Un fichier `.gitlab-ci.yml` est utilisé pour décrire les différentes étapes du pipeline. Le pipeline comprend les étapes suivantes :

1. Test : Cette étape utilise une image PHP pour exécuter les tests de l'application. Les dépendances du projet sont installées à l'aide de Composer, et les tests sont exécutés pour s'assurer que l'application fonctionne correctement.

2. Build : Cette étape pipeline construit une image Docker, la pousse vers un registre Docker, et archive un fichier de déploiement. Il est configuré pour être exécuté manuellement, ce qui signifie que vous devrez le déclencher manuellement depuis l'interface GitLab pour qu'il s'exécute.

3. Release Image : Cette étape est déclenchée uniquement lorsque le commit a un tag. Elle construit une nouvelle image Docker avec le tag de commit, la pousse vers le registre Docker et la rend disponible pour le déploiement ultérieur.

4. Deploy : Cette étape déploie l'application sur Kubernetes en utilisant le script de déploiement YAML. L'application est déployée dans l'espace de noms Kubernetes spécifié et devient accessible via l'ingress configuré .

Le pipeline d'intégration et de livraison continues est configuré pour s'exécuter automatiquement à chaque push sur la branche principale du référentiel GitLab.

## MicroK8s 

Pour le déploiement du projet, j'ai utilisé le cluster MicroK8s. MicroK8s est une distribution légère de Kubernetes qui permet de déployer facilement des applications en utilisant un ensemble réduit de ressources système. Le déploiement sur MicroK8s offre une solution rapide et fiable pour exécuter des applications Kubernetes sur des machines locales ou des environnements de développement.

De plus, j'ai créé un secret Kubernetes appelé my-secret en utilisant la commande kubectl create secret docker-registry. Ce secret est utilisé pour stocker les informations d'authentification nécessaires pour accéder au registre Docker lors du déploiement de l'image de l'application. Les informations d'authentification, telles que le nom d'utilisateur et le mot de passe, sont encodées et stockées de manière sécurisée dans le secret.

## Conclusion

En conclusion, ce projet de développement d'une application web simple a été un succès. J'ai utilisé le langage PHP et le framework Laravel pour construire une application fonctionnelle répondant à une requête HTTP de base. J'ai également conteneurisé l'application à l'aide de Docker, ce qui a facilité son déploiement et sa portabilité.

Le déploiement sur Kubernetes a été réalisé avec succès en utilisant MicroK8s, offrant un environnement robuste pour exécuter l'application. J'ai configuré un Ingress pour permettre l'accès à l'application depuis l'extérieur du cluster. De plus, j'ai mis en place un pipeline d'intégration et de livraison continues avec GitLab CI, ce qui a automatisé les tests, la construction de l'image Docker et le déploiement de l'application.