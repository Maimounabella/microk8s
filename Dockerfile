# Utiliser une image PHP avec Apache comme image de base
FROM php:8.1

# Définir le mainteneur
LABEL maintainer="maimouna bmaimouna338@gmail.com"

# Installer les dépendances nécessaires
RUN apt-get update \
    && apt-get install -y \
    libzip-dev \
    zip \
    git \
    && apt-get clean \
    && docker-php-ext-install zip 

# Définir le répertoire de travail
WORKDIR /my-simple-site

# Copier les fichiers de l'application
COPY ./my-simple-site/ /my-simple-site/

# Installer les dépendances de l'application avec Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer install --no-dev --optimize-autoloader  \
&& cp .env.example .env \
&& php artisan key:generate

# Exposer le port 80
EXPOSE 80

# Démarrer le serveur 
CMD ["php", "artisan", "serve", "--host=0.0.0.0", "--port=80"]